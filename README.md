# EOS.IO Node Environment #

This repository includes helpful files for building a single-server EOS.IO ecosystem.
This is helpful for SysAdmins who don't like using Docker and prefer to run bare-metal nodes.

Created by Caleb L. Power for use by EOS BlockSmith.

**Important** - these scripts are being phased out in favor of the Cruise Control Suite, and as such we realize that
these scripts are a bit crude. EOS BlockSmith does not offer technical support for these scripts... we only wanted
to share them for people who might find them useful. These scripts were influenced by
[CryptoLion's BP scripts](https://github.com/CryptoLions/scripts). This repository is licensed under the Apache Version 2 License.

## Files ##

* `eosiocmd` - a multi-purpose script that assists with the starting, stopping, and backup of node data
* `backup_all` - a helper script that backs up all known nodes in the background
* `upgrade` - a script that quickly upgrades the updated binaries

## Setup and Installation ##

Choose a working directory for the files to reside. In this directory, herein denoted `$HOME`, create three directories:

* eosio/bin
* eosio/src
* eosio/data
* tools/pitreos

Clone [pitreos from EOS Canada](https://github.com/eoscanada/pitreos.git) in the pitreos directory and build it.
The pitreos executable needs to be located in `$HOME/pitreos/pitreos`.
Modify the `eosiocmd` script to point to the appropriate backup locations. Read the pitreos documentation for more info.

Clone the source of an EOS.IO chain under some directory within the `$HOME/eosio/src` folder.
For example, if you're going to use the EOS Mainnet, clone the EOS Mainnet git repository under the folder `$HOME/eosio/src/eos-mainnet`.
Build the software using the `eosio_build.sh` script but *don't use the installation script*.

Create a directory under `$HOME/eosio/data` with the same name as the one you made in the `src` directory.
For example, if you created `$HOME/eosio/src/eos-mainnet`, also create `$HOME/eosio/data/eos-mainnet`.
Place the `genesis.json` and `config.ini` files in this folder. From here on, assume that `eos-mainnet` refers to the $INSTANCE directory.

If you happen to have a wallet (for claiming or the like), create a directory at `$HOME/eosio/data/$INSTANCE/wallet` and
place your wallet configuration file at `$HOME/eosio/data/$INSTANCE/wallet/config.ini`.

You can repeat the process of cloning and building for any number of nodes; just be sure that you comply with the various
constitutions, block producer agreements, and system requirements. If you build your EOS.IO nodes using the root user or another
privileged user, make sure that you `chown` your files back to your unprivileged EOSIO user (assuming you have one, for best practices,
`chown -R eosio:eosio *`). The scripts, of course, need to have the execution bit set (`chmod u+x <file>`).

You should use the `upgrade` script to copy the binary files that were built into the `$HOME/eosio/bin` directory. Make sure that a folder
denoting the $INSTANCE is already in `$HOME/eosio/bin`. For example, if you created `$HOME/eosio/src/eos-mainnet`, you also need to make
sure that you have `$HOME/eosio/bin/eos-mainnet`. Because the `bin` and `src` folders contain binary copies, you will be able to upgrade
future builds without interrupting your node for long periods of time.

## Utilizing the Scripts ##

Make sure that your current directory is `$HOME`. You can execute `./eosiocmd` to learn about the tool.
Usage: `./eosiocmd <instance> <domain> <command> [extra]`

Here are some common commands:

* Start the node for "eos-mainnet" - `./eosiocmd eos-mainnet node start`
* Stop the node for "eos-mainnet" - `./eosiocmd eos-mainnet node stop`
* Tail the log for "eos-mainnet" - `./eosiocmd eos-mainnet node getlog`
* Back up the node data for "eos-mainnet" - `./eosiocmd eos-mainnet node backup`
* Start the "eos-mainnet" wallet daemon - `./eosiocmd eos-mainnet wallet start`
* Stop the "eos-mainnet" wallet daemon - `./eosiocmd eos-mainnet wallet stop`
* Kick off "eos-mainnet" for the first time from `genesis.json` - `./eosiocmd eos-mainnet node start --genesis-json "$HOME/eosio/data/eos-mainnet/genesis.json"`
* Upgrade your node binaries for "eos-mainnet" after having previously built them - `./upgrade eos-mainnet`

Notes:

* Starting a node that's already running will result in a "restart"--the node will be stopped before starting
* The $INSTANCE variable relies on the directory structure... available instances are determined by `data` and `src` directory contents
* To backup all nodes in the background, simply execute `backup_all`.
* You'll have to use pitreos manually to restore backups.
* At some point, the `upgrade` and `backup_all` scripts will be wraped under the `eosio` script.
